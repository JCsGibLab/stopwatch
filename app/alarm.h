#ifndef ALARM_H
#define ALARM_H

#include <QObject>
#include <QDateTime>
#include <QTimer>

#include <QOrganizerCollection>
#include <QOrganizerManager>
#include <QOrganizerAbstractRequest>

QTORGANIZER_USE_NAMESPACE

class Alarm : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int millis READ millis WRITE setMillis NOTIFY millisChanged)
    Q_PROPERTY(bool running READ running NOTIFY runningChanged)
    Q_PROPERTY(bool paused READ paused NOTIFY pausedChanged)
public:
    explicit Alarm(QObject *parent = 0);

    int millis() const;
    void setMillis(int millis);

    bool running() const;
    bool paused() const;

public slots:
    void addSeconds(int seconds);
    void addMinutes(int minutes);
    void addHours(int hours);

    void start();
    void stop();
    void pause();

signals:
    void millisChanged();
    void runningChanged();
    void pausedChanged();

private:
    void startSync();
    void setupAlarm();
    void loadAlarms();

private slots:
    void fetchStateChanged(QOrganizerAbstractRequest::State state);
    void deleteStateChanged(QOrganizerAbstractRequest::State state);
    void writeStateChanged(QOrganizerAbstractRequest::State state);

    void update();

private:
    int m_millis;

    QOrganizerManager *m_manager;
    QOrganizerCollection m_collection;

    bool m_busy;
    bool m_restart;
    QDateTime m_startTime;
    QTimer m_timer;
    bool m_running;
};

#endif // ALARM_H
