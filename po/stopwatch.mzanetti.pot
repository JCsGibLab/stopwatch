# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-12 11:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: /home/arubislander/touch/stopwatch/app/Countdown.qml:203
#: /home/arubislander/touch/stopwatch/app/StopWatch.qml:79
msgid "Stop"
msgstr ""

#: /home/arubislander/touch/stopwatch/app/Countdown.qml:203
#: /home/arubislander/touch/stopwatch/app/StopWatch.qml:79
msgid "Clear"
msgstr ""

#: /home/arubislander/touch/stopwatch/app/Countdown.qml:221
msgid "Pause"
msgstr ""

#: /home/arubislander/touch/stopwatch/app/Countdown.qml:221
#: /home/arubislander/touch/stopwatch/app/StopWatch.qml:95
msgid "Start"
msgstr ""

#: /home/arubislander/touch/stopwatch/app/StopWatch.qml:95
#: /home/arubislander/touch/stopwatch/app/StopWatch.qml:164
msgid "Lap"
msgstr ""

#: /home/arubislander/touch/stopwatch/app/StopWatch.qml:157
msgid "Total"
msgstr ""
